import React, {Component} from 'react';
import Creator from './components/creator/creator'
import './App.scss';
import Product from './components/Product/product';
import Header from './components/header/header';
import Data from './assets/data.json';
import Footer from "./components/footer/footer";

export default class App extends Component {

    state = {
        cart: 0
    }

    handleCallback = () => {
        this.setState({cart: this.state.cart + 1});
    }

    render() {
        return (
            <div className='app'>
                <Header cartValue={this.state.cart}/>
                <div className='body'>
                    <Creator creatorData={Data.creator_statics}/>
                    <Product productData={Data.items} productDataLength={3} cartTitle={Data.cart_title} parentCallback={this.handleCallback}/>
                </div>
                <Footer/>
            </div>
    );
    }
    }
