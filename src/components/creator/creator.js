import React, {Component} from 'react';
import './creator.scss';

export default class Creator extends Component {

    data = this.props.creatorData;

    render() {
        return (
            <div className='creator-title'>
                <div className='banner'>
                    <img src={this.data.creator_banner_url} alt='banner'/>
                </div>
                <div className='creator'>
                    <div className='image-round'>
                        <img src={this.data.creator_icon_url} alt='creator'/>
                    </div>
                    <div className='detail'>
                        <div className='title-small'>
                            {this.data.creator_name}
                        </div>
                        <div className='description'>
                            {this.data.creator_subscriber_count} subscribers
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
