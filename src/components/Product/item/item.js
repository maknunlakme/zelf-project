import React, {Component} from 'react';
import './item.scss';
import Sign from './../../../assets/exclamation-sign.svg';
import downArrow from './../../../assets/down-arrow.svg';

export default class Item extends Component {

    data = this.props.data;
    quantity = 1;

    onTrigger = () => {
        this.props.parentCallback(this.data.sale_price_in_cents);
    }

    render() {
        return (
            <div className='item'>
                <div className='item-image'>
                    <img src={this.data.image} alt='item'/>
                </div>
                <div className='item-detail'>
                    <div className='other'>
                        <div className='description-brand'>
                            sold by <span className='description-bold'>{this.data.sold_by_brand}</span>
                            <img src={Sign} alt='sign'/>
                        </div>
                        <div className='title-very-small'>{this.data.name}</div>
                        <div className='description-color'>
                            <span>{this.quantity}</span>
                            <div className='dot'/>
                            <span><img src={this.data.color_thumbnail} alt='color'/></span>
                            <span className='color'>{this.data.color}</span>
                        </div>
                    </div>
                    <div className='price'>
                        {this.data.price_in_cents === this.data.sale_price_in_cents ? '' :
                            <div className='previous'><strike>${(this.data.price_in_cents / 100).toFixed(2)}</strike></div>}
                        <div className='price-button'>${(this.data.sale_price_in_cents / 100).toFixed(2)}</div>
                    </div>
                    <div className='buttons'>
                        <button>Buy Now</button>
                        <button onClick={this.onTrigger}>Add to Cart</button>
                        <img src={downArrow} alt='arrow'/>
                    </div>
                </div>
            </div>
        );
    }
}
