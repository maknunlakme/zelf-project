import React, {Component} from 'react';
import './product.scss';
import Item from "./item/item";

export default class Product extends Component {

    data = this.props.productData;
    length = this.props.productDataLength;
    title = this.props.cartTitle;
    state = {
        cart: 0,
        price: 0
    }

    handleCallback = (childData) => {
        this.setState({cart: this.state.cart + 1});
        this.setState({price: this.state.price + childData});
        this.props.parentCallback();
    }

    ComponentItems() {
        const items = [];
        for (let i = 0; i < this.length; i++) {
            items.push(
                <div key={this.data[i + 1].name}>
                    <Item data={this.data[i + 1]} parentCallback={this.handleCallback}/>
                </div>
            );
        }
        return <div>{items}</div>;
    }


    render() {
        return (
            <div className='product'>
                <div className='light-black-card top'>Select Items</div>
                <div className='card-title'>{this.title}</div>
                <div className='items'>{this.ComponentItems()}</div>
                <div className='light-black-card bottom'>Subtotal: <span className='float-right'>${(this.state.price / 100).toFixed(2)}</span>
                </div>
            </div>
        );
    }
}
