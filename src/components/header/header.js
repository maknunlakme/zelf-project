import React, {Component} from 'react';
import './header.scss';
import Cart from './../../assets/cart.svg';
import Logo from './../../assets/logo.svg';

export default class Header extends Component {

    render() {
        return (
            <div className='header'>
                <div className='title'>Zelf<span className='title-color-red'>.</span></div>
                <div><img src={Cart} alt='cart'/><button className='cart-value'>{this.props.cartValue}</button></div>
                <div><img src={Logo} alt='logo'/></div>
            </div>
        );
    }
}
